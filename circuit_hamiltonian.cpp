#include <iostream>
#include <fstream>

using namespace std;


//Citire matrice de adiacenta din fisier text
void citire(int &n, int a[20][20])
{
    ifstream fin("input.txt");
    fin >> n;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n; j++)
            fin >> a[i][j];
}


//Afisare matrice de adiacenta
void afisare_mat_adiac(int n, int a[20][20])
{
    cout << "\n";
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
            cout << a[i][j] << " ";
        cout << "\n";
    }
}


//Initializare nivel stiva
void init(int k, int st[])
{
    st[k] = 0;
}


//Verificare validitate succesor
int valid(int k, int st[], int a[20][20])
{
    int i;
    if (k > 1)
    {
        i = 1;
        while (st[i] != st[k] && (i < k))
            i++;
        if (i < k)
            return 0;
        return (a[st[k - 1]][st[k]]);
    }
    return 1;
}


//Verificare daca stiva este completata si drumul rezultat este un circuit
int solutie(int k, int n, int st[], int a[20][20])
{
    return ((k == n) && (a[st[n]][st[1]]));
}


//Verificare existenta succesor
int am_succesor(int n, int k, int st[])
{
    if (st[k] < n)
    {
        st[k]++;
        return 1;
    }
    return 0;
}



//Afisare solutie
void tipar(int n, int st[])
{
    cout << "\n";
    for (int i = 1; i <= n; i++)
        cout << st[i] << " ";
    cout << st[1];
}


//Aplicarea metodei backtracking
void backtracking(int st[], int n, int k, int a[20][20])
{
    bool exista = false;
    int as;
    k = 1;
    init(k, st);
    while (k > 0)
    {
        do{} while ((as = am_succesor(n, k, st)) && !valid(k, st, a));
        if (as)
            if (solutie(k, n, st, a))
            {
                tipar(n, st);
                exista = true;
            }
            else
            {
                k++;
                init(k, st);
            }
        else
            k--;
    }
    if (!exista)
    {
        cout << "\nGraful nu contine circuit hamiltonian\n";
    }

}


//Cautare circuite Hamiltoniene
void circuit_hamiltonian()
{
    int st[100], n, k, a[20][20];
    citire(n, a);
    cout << "\n\nMatricea de adiacenta a grafului este:\n";
    afisare_mat_adiac(n, a);
    cout << "\nCircuitele hamiltoniene din graf sunt:\n";
    backtracking(st, n, k, a);
}


int main()
{
    circuit_hamiltonian();
    cout << "\n";
    return 0;
}