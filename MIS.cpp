#include <iostream>
#include <fstream>

using namespace std;


//Citire matrice de adiacenta din fisier text
void citire(int &n, int a[20][20])
{
    ifstream fin("input.txt");
    fin >> n;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n; j++)
            fin >> a[i][j];
}


//Afisare matrice de adiacenta
void afisare_mat_adiac(int n, int a[20][20])
{
    cout << "\n";
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
            cout << a[i][j] << " ";
        cout << "\n";
    }
}


//Initializare nivel stiva
void init(int k, int st[])
{
    st[k] = 0;
}


//Verificare existenta succesor
int am_succesor(int n, int k, int st[])
{
    if (st[k] < n)
    {
        st[k]++;
        return 1;
    }
    return 0;
}


//Verificare validitate succesor
int valid(int k, int st[], int a[20][20])
{
    int i;
    if (k > 1)
    {
        i = 1;
        while (st[i] < st[k] && (i < k) && a[st[i]][st[k]] == 0 && a[st[k]][st[i]] == 0)
            i++;
        if (i < k)
            return 0;
        return 1;
    }
    return 1;
}


//Afisare solutie
void tipar(int n, int st[])
{
    cout << "\n{";
    for (int i = 1; i <= n; i++)
    {
        if (i > 1)
            cout << ", ";
        cout << st[i];
    }
    cout << "}";
}



//Memorare multime interior stabila maxima obtinuta pana la momentul respectiv
void salveaza(int v[], int length, int st[])
{
    for (int i = 1; i <= length; i++)
    {
        v[i] = st[i];
    }
}


//Aplicarea metodei backtracking
void backtracking(int st[], int n, int k, int a[20][20])
{
    int as;
    k = 1;
    int k_max = 0;
    int st_max[100];
    init(k, st);
    while (k > 0)
    {
        do{} while ((as = am_succesor(n, k, st)) && !valid(k, st, a));
           if (as)
        {
            tipar(k, st);
            if (k_max < k)
            {
                k_max = k;
                salveaza(st_max, k_max, st);
            }
            k++;
            init(k, st);
        }
        else
            k--;
    }
    cout << "\n\nNumarul de stabilitate interna a grafului este: " << k_max;
    cout << "\n\nO multime interior stabila maxima este: ";
    tipar(k_max, st_max);
}


//Determinare multimi interior stabile
void mis()
{
    int st[100], n, k, a[20][20];
    citire(n, a);
    cout << "\n\nMatricea de adiacenta a grafului este:\n";
    afisare_mat_adiac(n, a);
    cout << "\n MIS sunt:\n";
    backtracking(st, n, k, a);
}


int main()
{
    mis();
    cout << "\n";
    return 0;
}