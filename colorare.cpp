#include <iostream>
#include <fstream>

using namespace std;


//Citire matrice de adiacenta din fisier text
void citire(int &n, int a[20][20])
{
    ifstream fin("input.txt");
    fin >> n;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n; j++)
            fin >> a[i][j];
}


//Afisare matrice de adiacenta
void afisare_mat_adiac(int n, int a[20][20])
{
    cout << "\n";
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
            cout << a[i][j] << " ";
        cout << "\n";
    }
}


//Initializare nivel stiva
void init(int k, int st[])
{
    st[k] = 0;
}


//Verificare existenta succesor
int am_succesor(int n, int k, int st[])
{
    if (st[k] < 4)
    {
        st[k]++;
        return 1;
    }
    return 0;
}


//Verificare daca stiva este completata
int solutie(int k, int n)
{
    return (k == n);
}


//Afisare solutie
void tipar(int n, int st[])
{
    cout << "\n";
    for (int i = 1; i <= n; i++)
    {
        //cout << st[i] << " ";
        switch(st[i]){
            case 1 : cout << "Verde "; break;
            case 2 : cout << "Rosu "; break;
            case 3 : cout << "Albastru "; break;
            case 4 : cout << "Mov "; break;
        }
    }
}


//Verificare validitate succesor
int valid(int k, int st[], int a[20][20])
{
    int i;
    if (k > 1)
    {
        for (int i = 1; i <= k; i++)
        {
            if (a[k][i] == 1 || a[i][k] == 1)
            {
                if (st[k] == st[i])
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}


//Aplicarea metodei backtracking
void backtracking(int st[], int n, int k, int a[20][20])
{
    int as;
    k = 1;
    init(k, st);
    while (k > 0)
    {
        do{} while ((as = am_succesor(n, k, st)) && !valid(k, st, a));
        if (as)
            if (solutie(k, n))
                tipar(n, st);
            else
            {
                k++;
                init(k, st);
            }
    else
        k--;
    }
}


//Determinare moduri de colorare cu patru culori a unei harti date
void colorare_harti()
{
    int st[100], n, k, a[20][20];
    citire(n, a);
    cout << "\n\nMatricea de adiacenta a grafului este:\n";
    afisare_mat_adiac(n, a);
    cout << "\nHarta rezultata se coloreaza astfel:\n";
    backtracking(st, n, k, a);
}


int main()
{
    colorare_harti();
    cout << "\n";
    return 0;
}